package loanmanagement.controller;

import java.sql.*;

public class DatabaseController {

    private static DatabaseController databaseController = null;
    private String url;
    private String username;
    private String password;
    private String database;
    private Connection connection;
    private PreparedStatement preparedSql;
    private ResultSet resultSet;

    public static DatabaseController getInstance() {
        if (databaseController == null)
            databaseController = new DatabaseController();
        return databaseController;
    }

    // Initialize the Connection with the Database
    public void initializeSQLConnection() {
        try {
            // DriverManager.getConnection:: Attempts to establish a connection to the given database URL.
             if (connection == null) connection = DriverManager.getConnection(getUrl(), getUsername(), getPassword());
            System.out.println("Database connected!");
        } catch (SQLException e){
            System.out.println("There was an error connecting to the database: " + e);
        }
    }

    // Close SQL Connection
    public void closeSQLConnection() {
        if (getConnection() != null) {
            try {
                getConnection().close();
                System.out.println("Connection Closed!");
            } catch (SQLException sqlEx) {
                // TODO: Throw an exception?
                System.out.println("Error closing SQL Connection: " + sqlEx);
            }
        }

        if (getPreparedSql() != null) {
            try {
                getPreparedSql().close();
                System.out.println("Prepared Statement Closed!");
            } catch (Exception e) {
                // TODO: Throw an exception?
                System.out.println("Error closing Prepared Statement " + e);
            }
        }

        if (getResultSet() != null) {
            try {
                getResultSet().close();
                System.out.println("Result Set has been closed!");
            }catch(SQLException e){
                // TODO: Verify this is correct implementation of throw?
                throw new IllegalStateException("Cannot connect the data base!", e);
            }
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public Connection getConnection() {
        return connection;
    }


    public PreparedStatement getPreparedSql() {
        return preparedSql;
    }

    public void setPreparedSql(PreparedStatement preparedSql) {
        this.preparedSql = preparedSql;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
}
