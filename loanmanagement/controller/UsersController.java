package loanmanagement.controller;

import loanmanagement.model.Users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

public class UsersController {
    private String sqlStatement = "";
    private static DatabaseController _dbController = DatabaseController.getInstance();

    // Add User SQL Query
    // TODO: Throw  Error if SQLIntegrityContraintViolationException occurs that is NOT IllegalStateException
    // Note: Adding a duplicate username WILL increment user_id.
    // https://stackoverflow.com/questions/14758625/mysql-auto-increment-columns-on-transaction-commit-and-rollback/14758690#14758690

     /**
     * This methods adds a new user to the user table
     * @param user as defined in the Users class
     */
    public void addNewUser(Users user) {
        try {
            sqlStatement = "INSERT INTO Users" +
                    "(" +
                    "username," +
                    "firstname," +
                    "lastname," +
                    "password" +
                    ")" +
                    "VALUES" +
                    "(" +
                    "'" + user.getUsername() +"'," +
                    "'" + user.getFirstName() + "'," +
                    "'" + user.getLastName() + "'," +
                    "'" + user.getPassword() + "'" +
                    ");";
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            int i = _dbController.getPreparedSql().executeUpdate();
            if (i > 0) {
                System.out.println("Successfully added new user: " + user.getUsername());
            } else {
                System.out.println("Unable to add user: " + user.getUsername());
            }
        } catch (SQLException e) {
            if(e instanceof SQLIntegrityConstraintViolationException){
                System.out.println("Username already exists in DB.");
            } else
                throw new IllegalStateException(e);
        }
    }

    /**
     * Remove User by ID SQL Query
     * @param userId the userId to be removed
     */
    public void removeUserByUserID(String userId){
        try {
            sqlStatement = "DELETE FROM Users WHERE user_id = '" + userId + "';";
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            int i = _dbController.getPreparedSql().executeUpdate();
            if (i > 0) {
                System.out.println("Successfully removed user_id: " + userId);
            } else {
                System.out.println("Unable to remove user_id: " + userId);
            }

        } catch(SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Remove by Username;
      * @param username to be removed
     */
    public void removeUserByUserName(String username) {
        try {
            sqlStatement = "DELETE FROM Users WHERE username = '" + username + "';";
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            int i = _dbController.getPreparedSql().executeUpdate();
            if (i > 0) {
                System.out.println("Successfully removed username: " + username);
            } else {
                System.out.println("Unable to remove username: " + username + ".");
            }

        } catch(SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Grab User info by userName
     * @param username to be used to grab information about
     * @return returns a result set of all the data in the table regarding this user (FirstName, LastName, UserName, Password)
     */
    public ResultSet selectUserByUsername(String username){
        try {
            sqlStatement = "SELECT * FROM Users WHERE username = '" + username + "';";
            // This sets the prepared statement prior to executing the code
            // Possible to reduce lines of code by removing the setPreparedSql and simply calling _dbController.getConnection().prepareStatement(sqlStatement).executeUpdate();
            // Would that be advantageous to do, or better to keep as is?
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));

            _dbController.setResultSet(_dbController.getPreparedSql().executeQuery());

            //_dbController.getResultSet().next();



        }catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return _dbController.getResultSet();
    }

    /**
     *  Grab User info by userId
     * @param userid to be used to grab information about
     * @return returns a result set of all the data in the table regarding this user (FirstName, LastName, UserName, Password)
     */
    public ResultSet selectUserByUserId(String userid){
        try {
            sqlStatement = "SELECT * FROM Users WHERE user_id = '" + userid + "';";
            // This sets the prepared statement prior to executing the code
            // Possible to reduce lines of code by removing the setPreparedSql and simply calling _dbController.getConnection().prepareStatement(sqlStatement).executeUpdate();
            // Would that be advantageous to do, or better to keep as is?
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));

            _dbController.setResultSet(_dbController.getPreparedSql().executeQuery());

           // _dbController.getResultSet().next();

        }catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return _dbController.getResultSet();
    }
}
