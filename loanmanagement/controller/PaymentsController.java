package loanmanagement.controller;

import loanmanagement.model.Debts;
import sun.plugin.dom.exception.InvalidStateException;

import javax.xml.transform.Result;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PaymentsController {

    private DatabaseController _dbController = DatabaseController.getInstance();;


    /**
     * This method handles adding a new payment record
     * @param debt is the Debts class that contains all the data regarding this debt record
     * @param debtId is the Debts Class variable, debtId
     * @throws ParseException
     */
    public void addPaymentRecord(Debts debt, String debtId) throws ParseException {
        String sqlStatement = "";
        try {
            sqlStatement = "INSERT INTO payments(" +
                    "user_id," +
                    "debt_id," +
                    "date_due," +
                    "amount_due" +
                    ") VALUES (" +
                    debt.getUserId() + "," +
                    debtId + ", " +
                    "\'" + calculateDateDue(debt.getStartDate(), debt.getPaymentInterval()) + "\'," +
                    calculateAmountDue(debt) + ");";
            System.out.println(sqlStatement);
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            int i = _dbController.getPreparedSql().executeUpdate();
            if (i > 0){
                System.out.println("Added to Payments table correctly!");
            } else System.out.println("Failed ot add to payments table....");

        } catch (Exception e){
            if(e.getClass() == SQLException.class)
                throw new InvalidStateException(e.getMessage());
            if(e.getClass() == ParseException.class)
                throw new InvalidStateException("Goofed up adding a month..");
            throw new InvalidStateException("wtf!" + e.getMessage());
        }

    }

    // TODO: Redo this using the SQL Query "SELECT * FROM payments WHERE user_id = userId AND debt_id = debtId;"
    // TODO: Use the results of this query to save the amount_due to a variable
    // TODO: Lower the "current_debt" value in Debts

    /**
     * This method will handle making a payment to a specified user, debt, and the amount paid
     * @param debtId is the specific debt that we are making a payment to
     * @param userId is the specific user that owns the debt
     * @param amountPaid is how much the user is paying
     */
    public void makePayment(String debtId, String userId, double amountPaid){
        String sqlStatement = "";

        Debts updatedDebtRecord = null;
        try {
            sqlStatement = "UPDATE payments " +
                    "SET date_paid = " + new java.sql.Date(new java.util.Date().getTime()) + "," +
                    "amount_paid = " + amountPaid +
                    "WHERE user_id = " + userId + " AND debt_id = " + debtId + ";";

            // UPDATE THE CURRENT PAYMENT RECORD
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            _dbController.getPreparedSql().executeUpdate();

            // Grab Current Debt Record
            sqlStatement = "SELECT * FROM debts WHERE user_id = " + userId + " AND debt_id = " + debtId + ";";
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));

            ResultSet debtQueryResults = _dbController.getPreparedSql().executeQuery();
            debtQueryResults.next();

            double currentDebt = debtQueryResults.getDouble("current_debt");

            double remainingDebt = currentDebt - amountPaid;

            if(remainingDebt < 0)
                throw new InvalidStateException("Paid off too much.  We would owe you money. ");

            updatedDebtRecord = new Debts(debtQueryResults.getString("debt_id"),
                    debtQueryResults.getString("user_id"),
                    debtQueryResults.getDouble("original_debt"),
                    remainingDebt,
                    debtQueryResults.getString("start_date"),
                    debtQueryResults.getInt("loan_length"),
                    debtQueryResults.getInt("payment_interval"));

            // UPDATE THE DEBTS TABLE AND SET current_debt = remainingDebt
            sqlStatement = "UPDATE debts SET current_debt = " + remainingDebt + " WHERE user_id = " + userId + " AND debt_id = " + debtId + ";";
            _dbController.setPreparedSql(_dbController.getConnection().prepareStatement(sqlStatement));
            _dbController.getPreparedSql().executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("SQL Query issues.... " + e.getMessage());
        }
        try {
            addPaymentRecord(updatedDebtRecord, debtId);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifies that the user owns the debt
     * @param debtId is the debtId from table debts
     * @param userId is the userId from table users
     * @return a boolean flag indicating whether or not the debt belongs to the user
     */
    private boolean verifyUserOwnsDebt(String debtId, String userId){
        DebtsController debtsController = new DebtsController();
        try {
            Debts[] debtsArray = debtsController.selectAllDebtIDsByUserId(userId);
            for (Debts debt:debtsArray) {
                if(debt.getDebtId().equals(debtId))
                    return true;
            }
        } catch (DebtsController.NoDebtsFoundForUserException e) {
            e.printStackTrace();
            throw new InvalidStateException("No debts found");
        }
        return false;
    }

    /**
     * This method checks to make sure the user exists-- assume the user does not exist
     * @param userId the id of the user we are attempting to cehck
     * @return a boolean flag
     */
    private boolean verifyUserIdExists(String userId){
        // Select user_id from users where user_id = userId;
        UsersController userController = new UsersController();
        // Look for userId
        _dbController.setResultSet(userController.selectUserByUserId(userId));

        try {
            // Should return true if the userid exists in the User table, false otherwise
            return _dbController.getResultSet().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // should return false if it does not exist
        return false;
    }

    // TODO: Complete this method.
    private void selectAllDebtIdForUserId(String userId){
        DebtsController debtsController = new DebtsController();

    }

    /**
     * Calculates the due date based on the conditions supplied
     * @param date is the currentDate
     * @param loanInterval how often we must make payments
     * @return the loan's due date.
     * @throws ParseException
     */
    private String calculateDateDue(String date, int loanInterval) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(date));
        c.add(Calendar.MONTH, 1);

        return sdf.format(c.getTime());
    }

    /**
     * Calculates the amount that is due in the next payment
     * @param debt is the debt record we're working with
     * @return a double of the next payment.
     */
    private double calculateAmountDue(Debts debt){
        double AmountDue = 0.0;
        double currentDebt = debt.getCurrentDebt();
        AmountDue = currentDebt / (debt.getLoanLength() / debt.getPaymentInterval());
        return AmountDue;
    }

}

