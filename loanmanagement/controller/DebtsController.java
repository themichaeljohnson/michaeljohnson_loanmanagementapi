package loanmanagement.controller;

import loanmanagement.model.Debts;
import sun.plugin.dom.exception.InvalidStateException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DebtsController {

    private DatabaseController dbController = DatabaseController.getInstance();
    private final String startDate = initializeStartDate();

    /**
     * The SQL Query should only be able to add the new debt record IF AND ONLY IF the user_id exists within
     * debtId, userId, dueDate, datePaid = NULL, amountDue, amountPaid = NULL
     * Creates new debt records based on Debts class definition
     * @param debt
     */
    public void addDebtRecord(Debts debt) {
        //String sqlStatement = "";

        try {

            final String sqlStatement = "INSERT INTO Debts" +
                    "(" +
                    "user_id," +
                    "original_debt," +
                    "current_debt," +
                    "start_date," +
                    "loan_length," +
                    "payment_interval" +
                    ")SELECT user_id, ?, ?, ?, ?, ? from users where user_id = ?;";

            dbController.setPreparedSql(dbController.getConnection().prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS));
            dbController.getPreparedSql().setDouble(1, debt.getOriginalDebt());
            dbController.getPreparedSql().setDouble(1, debt.getCurrentDebt());
            dbController.getPreparedSql().setString(1, debt.getStartDate());
            dbController.getPreparedSql().setInt(1, debt.getLoanLength());
            dbController.getPreparedSql().setInt(1, debt.getPaymentInterval());

            int i = dbController.getPreparedSql().executeUpdate();
            if (i > 0) {
                System.out.println("Successfully added a new debt record....");
            } else {
                System.out.println("Record was not added to the Debts Table. User likely does not exist");
            }
        }catch (SQLException e) {
            //System.out.println(sqlStatement);
            throw new IllegalStateException(e);
        } // end catch
        PaymentsController paymentsController = new PaymentsController();


        try {

            ResultSet rs = dbController.getPreparedSql().getGeneratedKeys();
            rs.next();
            String debtId = rs.getString(1);
            paymentsController.addPaymentRecord(debt,debtId);
        } catch (Exception e){
            //TODO: fix this mess
            if(e.getClass().equals(ParseException.class.toString()))
                throw new InvalidStateException("Incrementing the month failed.");
            if(e.getClass().equals(SQLException.class.toString()))
                throw new InvalidStateException("Oops.");
            throw new InvalidStateException("BIG OOPS. " + e.getMessage());
        }
    }

    /**
     *  calculateTotalDebt which will query "debt" and collect all user_id's and current_debt from DEBTS and add them together
     *  This method requires the userId to be passed in and will prepare a select statement using that userId
     *  We'll query debts for all debts with that userId and add each one to a variable titled "totalDebt" and return that
     *  We'll create and execute 2 separate statements using the userId
     * @param userId that we want to use to calculate the total debt for
     * @return a double of all the users debts added up
     */
    public double calculateTotalDebt(String userId){
        double totalDebt = 0;

        String sqlStatement = "";
        String sqlStatementSize = "";
        try {

            // This is the first statement, we're simply getting the number of records that return using the user id
            sqlStatementSize = "SELECT count(*) as rowCount FROM debts where user_id = " + userId ;
            dbController.setPreparedSql(dbController.getConnection().prepareStatement(sqlStatementSize));
            dbController.setResultSet(dbController.getPreparedSql().executeQuery());
            int rowCount = 0;
            while(dbController.getResultSet().next()) {
                rowCount = dbController.getResultSet().getInt("rowCount");
            }
            System.out.println(rowCount);

            // this is the second statement, we're just grabbing each records current_debt and dropping them into an array of doubles
            sqlStatement = "SELECT current_debt FROM debts where user_id = " + userId ;
            dbController.setPreparedSql(dbController.getConnection().prepareStatement(sqlStatement));
            dbController.setResultSet(dbController.getPreparedSql().executeQuery());

            double[] resultSetArray = new double[rowCount];
            int current = 0;
            while(dbController.getResultSet().next()) {
                resultSetArray[current] = dbController.getResultSet().getDouble("current_debt");
                current++;
            }

            // finally we'll iterate through that array and simply add each value to the original "totalDebt" variable
            for (double result: resultSetArray) {
                totalDebt += result;
            }
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return totalDebt;
    }

    /**
     * Grabs all debts for single user by user_id and returns a Array of Debt Objects size of the number of debts for that user
     * @param userId userId we're looking for in the debts table
     * @return an array of Debts that we can then use to display all the debts for a specified user
     * @throws NoDebtsFoundForUserException
     */
    public Debts[] selectAllDebtIDsByUserId(String userId) throws NoDebtsFoundForUserException {
        String sqlStatement = "";
        int numberOfDebts = 0;
        try{
            sqlStatement = "SELECT COUNT(*) FROM debts WHERE user_id = (SELECT user_id FROM users where user_id = " + userId + ");";
            ResultSet userCountResultSet = dbController.getConnection().prepareStatement(sqlStatement).executeQuery();
            userCountResultSet.next();
            numberOfDebts = userCountResultSet.getInt("count(*)");
        }catch(SQLException e){
            throw new IllegalStateException(e);
        }

        Debts[] debtArray = new Debts[numberOfDebts];
        try {
            // This SQL Statement will only allow you to attempt to select data from debts when the user_id exists in users
            sqlStatement = "SELECT * FROM debts WHERE user_id = (SELECT user_id FROM users where user_id = " + userId + ");";
            dbController.setPreparedSql(dbController.getConnection().prepareStatement(sqlStatement));
            ResultSet userDebtListResultSet = dbController.getPreparedSql().executeQuery();

            for(int i = 0; i < numberOfDebts; i++){
                userDebtListResultSet.next();
                debtArray[i] = new Debts(userDebtListResultSet.getString("debt_id"),
                                         userDebtListResultSet.getString("user_id"),
                                         userDebtListResultSet.getDouble("original_debt"),
                                         userDebtListResultSet.getDouble("current_debt"),
                                         userDebtListResultSet.getString("start_date"),
                                         userDebtListResultSet.getInt("loan_length"),
                                         userDebtListResultSet.getInt("payment_interval"));
            } // end for loop
            if (debtArray.length == 0)
                throw new NoDebtsFoundForUserException("No Debt associated with user_id.");
        } catch (SQLException e)
        {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }

        return debtArray;
    }

    /**
     * Used to initialize the start date when generating a new debt
     * @return The Day in yyyy-MM-dd format
     */
    private String initializeStartDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        return sdf.format(c.getTime());
    }

    // TODO: Get rid of this...
    public class NoDebtsFoundForUserException extends Throwable {
        public NoDebtsFoundForUserException(String Message)
        {
            super(Message);
        }
    }
}
