package loanmanagement;

import loanmanagement.controller.DebtsController;
import loanmanagement.controller.PaymentsController;
import loanmanagement.controller.UsersController;
import loanmanagement.model.Debts;
import loanmanagement.model.Payments;
import loanmanagement.model.Users;
import loanmanagement.controller.DatabaseController;

public class Main {
    public static void main(String[] args) {
        DatabaseController test = DatabaseController.getInstance();
        try {
            test.setUrl("jdbc:mysql://localhost:3306/loanmanagement");
            test.setUsername("test");
            test.setPassword("test");
            test.initializeSQLConnection();

//            Users user = new Users("MichaelJ", "michaelj", "ljohnson", "test1");
//            UsersController usersController = new UsersController();
//            usersController.addNewUser(user);


            DebtsController debtsController = new DebtsController();
//            Debts[] debtArray = null;
//            try {
//                debtArray = debtsController.selectAllDebtIDsByUserId("8");
//
//            } catch (DebtsController.NoDebtsFoundForUserException e) {
//                e.printStackTrace();
//            }
//
//            if(debtArray != null) {
//                for (Debts debt : debtArray) {
//                    System.out.println("============================");
//                    System.out.println("User ID:\t" + debt.getUserId());
//                    System.out.println("Debt ID:\t" + debt.getDebtId());
//                    System.out.println("Original Debt:\t" + debt.getOriginalDebt());
//                    System.out.println("Current Debt:\t" + debt.getCurrentDebt());
//                    System.out.println("Start Date:\t" + debt.getStartDate());
//                    System.out.println("============================");
//                }
//            }

            //Debts debt = new Debts("1", 301, 301, new DebtsController().initializeStartDate() , 3, 1);
            //debtsController.addDebtRecord(debt);
//            Debts debt2 = new Debts("8", 302, 301, 3, 1);
//            Debts debt3 = new Debts("8", 301, 301, 3, 1);


            PaymentsController paymentsController = new PaymentsController();
            paymentsController.makePayment("1", "1", 100.33);
            //paymentsController.makePayment("2", "1", 5);
            //System.out.println(paymentsController.calculateDateDue("2018-11-13"));

        } catch (Exception e) {
           throw new IllegalStateException(e);
        }

        finally{
            test.closeSQLConnection();
        }



    }
}
