package loanmanagement.model;

public class Users {
    private final String userId;
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String password;

    // This constructor is used for INSERTING data
    public Users(String username, String firstName, String lastName, String password){
        userId = "";
        this.username  = username;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.password  = password;
    }

    // This constructor is used for PULLING data
    public Users(String userId, String username, String firstName, String lastName, String password){
        this.userId = userId;
        this.username  = username;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.password  = password;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

}
