package loanmanagement.model;

import loanmanagement.controller.DatabaseController;

public class Payments {
    private String debtId;
    private String userId;
    final private String dueDate;
    final private String datePaid;
    final private double amountDue;
    final private double amountPaid;


    public Payments(String dueDate, String datePaid, double amountDue, double amountPaid) {
        this.dueDate = dueDate;
        this.datePaid = datePaid;
        this.amountDue = amountDue;
        this.amountPaid = amountPaid;
    }

    public String getDebtId() {
        return debtId;
    }

    public String getUserId() {
        return userId;
    }

    public String getDueDate() {
        return dueDate;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public double getAmountDue() {
        return amountDue;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

}
