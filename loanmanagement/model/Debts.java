package loanmanagement.model;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Debts {
    private String debtId;
    private final String userId;
    private final double originalDebt;
    private final double currentDebt;
    private final String startDate;
    private final int loanLength;
    private final int paymentInterval;

    /**
     * This Constructor is used when a new Debt is being added to the database
     * @param userId is the userId the debt will be associated with
     * @param originalDebt is the original amount of the debt
     * @param currentDebt is the current amount of the debt
     * @param loanLength is how loan term length
     * @param paymentInterval is how often the user would like to make payments (in months)
     */
    public Debts(String userId, double originalDebt, double currentDebt, int loanLength, int paymentInterval) {
        this.userId          = userId;
        this.originalDebt    = originalDebt;
        this.currentDebt     = currentDebt;
        this.startDate       = initializeStartDate();
        this.loanLength      = loanLength;
        this.paymentInterval = paymentInterval;
    }

    // Grabbing Data
    public Debts(String debtId, String userId, double originalDebt, double currentDebt, String startDate, int loanLength, int paymentInterval) {
        this.debtId          = debtId;
        this.userId          = userId;
        this.originalDebt    = originalDebt;
        this.currentDebt     = currentDebt;
        this.startDate       = startDate;
        this.loanLength      = loanLength;
        this.paymentInterval = paymentInterval;
    }

    // Getters for Debts variables
    public String getDebtId() {
        return debtId;
    }

    public String getUserId() {
        return userId;
    }

    public double getOriginalDebt() {
        return originalDebt;
    }

    public double getCurrentDebt() {
        return currentDebt;
    }

    public String getStartDate() {
        return startDate;
    }

    public int getLoanLength() {
        return loanLength;
    }

    public int getPaymentInterval() {
        return paymentInterval;
    }

    private String initializeStartDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        return sdf.format(c.getTime());

    }
}
